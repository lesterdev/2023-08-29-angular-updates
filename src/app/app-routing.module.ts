import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'standalone',
    loadComponent: async () => import('./pages/standalone.component'),
  },
  {
    path: 'input/:countId',
    loadComponent: async () => import('./pages/router-inputs.component'),
  },
  {
    path: 'signal',
    loadComponent: async () => import('./pages/signal.component'),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      bindToComponentInputs: true,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
