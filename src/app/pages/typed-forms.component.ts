import { Component } from '@angular/core';
import {
  UntypedFormBuilder,
  FormBuilder,
  NonNullableFormBuilder,
} from '@angular/forms';

@Component({
  standalone: true,
  imports: [],
  template: ` <div></div> `,
})
export default class StandaloneComponent {
  untypedForm = this.untyped.group({
    value1: [''],
    value2: [0],
  });

  fbForm = this.fb.group({
    value1: [''],
    value2: [0],
  });

  nonNullForm = this.nonNull.group({
    value1: [''],
    value2: [0],
  });

  constructor(
    private readonly untyped: UntypedFormBuilder,
    private readonly fb: FormBuilder,
    private readonly nonNull: NonNullableFormBuilder
  ) {}
}
