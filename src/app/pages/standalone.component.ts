import { CommonModule, NgIf } from '@angular/common';
import { Component } from '@angular/core';
import RequiredInputsComponent from './required-inputs.component';

@Component({
  standalone: true,
  imports: [CommonModule, RequiredInputsComponent],
  template: `
    <div>
      {{ title }}
      <div *ngIf="name">Some true data</div>
      <required-input [count]="12" />
    </div>
  `,
})
export default class StandaloneComponent {
  title = '2023-08-29-angular-updates';
  name = true;
}
