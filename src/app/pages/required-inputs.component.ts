import { CommonModule, NgIf } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  standalone: true,
  imports: [],
  template: ` <div>count: {{ count }}</div> `,
  selector: 'required-input',
  // inputs: [
  //   {
  //     name: 'count',
  //     required: true,
  //   },
  // ],
})
export default class RequiredInputsComponent {
  @Input({ required: true }) public count!: number;
}
