import { AsyncPipe, CommonModule, JsonPipe, NgIf } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  DestroyRef,
  Input,
  OnInit,
  ViewChild,
  computed,
  signal,
} from '@angular/core';
import { Subject, map, takeUntil } from 'rxjs';
import {
  toSignal,
  toObservable,
  takeUntilDestroyed,
} from '@angular/core/rxjs-interop';

interface MyObject {
  value1: number;
  value2: string;
}

@Component({
  standalone: true,
  imports: [AsyncPipe, JsonPipe],
  template: ` <div>count: {{ count() }} | count$: {{ count$ | async }}</div>
    <button (click)="upDown(1)">up</button>
    <button (click)="upDown(-1)">down</button>
    <div>finalValue: {{ finalValue() }}</div>
    <div>number: {{ number() }}</div>
    <div>objectSignal: {{ objectSignal()?.value1 }}</div>
    <div>{{ objectSignal() | json }}</div>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class SignalComponent implements OnInit {
  count = signal<number>(0);
  count$ = toObservable(this.count);

  multiplier = signal(2).asReadonly();

  finalValue = computed(() => this.count() * this.multiplier());

  numberSubject$ = new Subject<number>();
  number$ = this.numberSubject$.asObservable();

  number = toSignal(this.number$);

  objectSignal = signal<null | MyObject>(null);

  // public upDown(num: number): void {
  //   setTimeout(() => {
  //     this.count = this.count += num;
  //     console.log(this.count);
  //     this.cdr.markForCheck();
  //   }, 100);
  // }

  public upDown(num: number): void {
    setTimeout(() => {
      this.count.set(this.count() + num);
      this.numberSubject$.next(this.count());
      this.objectSignal.set({
        value1: this.count(),
        value2: this.count().toString(),
      });
    }, 100);
  }

  constructor(private cdr: ChangeDetectorRef, private destroyRef: DestroyRef) {}

  public ngOnInit(): void {
    this.count$
      .pipe(
        map((num) => num * 5),
        map(console.log),
        takeUntilDestroyed(this.destroyRef)
      )
      .subscribe();
  }
}
